const mapObject = require("../mapObject");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

function cb(value) {
    return value;
}
const result = mapObject(testObject, cb);

console.log(result);
