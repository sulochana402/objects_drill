function mapObject(obj, cb) {
  if (typeof obj === "object") {
    let transformedObject = {};
    for (let keys in obj) {
      transformedObject[keys] = cb(obj[keys]);
    }
    return transformedObject;
  } else {
    return {};
  }
}
module.exports = mapObject;
