function invert(obj) {
  if (typeof obj === "object") {
    let invertedObject = {};
    for (let key in obj) {
      invertedObject[obj[key]] = key;
    }
    return invertedObject;
  } else {
    return {};
  }
}
module.exports = invert;
