function values(obj) {
  if (typeof obj === "object") {
    let newArray = [];
    for (let values in obj) {
      newArray.push(obj[values]);
    }
    return newArray;
  }
}
module.exports = values;
