function pairs(obj) {
  if (typeof obj === "object") {
    let pairsArray = [];
    for (let key in obj) {
      pairsArray.push([key, obj[key]]);
    }
    return pairsArray;
  } else {
    return {};
  }
}
module.exports = pairs;
