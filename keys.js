function keys(obj) {
  if (typeof obj === "object") {
    let newArray = [];
    for (let keys in obj) {
      newArray.push(keys);
    }
    return newArray;
  } else {
    return {};
  }
}
module.exports = keys;
