function defaults(obj, defaultProps) {
  if (typeof obj === "object") {
    for (let key in defaultProps) {
      if (obj[key] === undefined) {
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  } else {
    return {};
  }
}
module.exports = defaults;
